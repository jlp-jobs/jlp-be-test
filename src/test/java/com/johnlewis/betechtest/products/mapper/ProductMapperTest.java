package com.johnlewis.betechtest.products.mapper;

import com.johnlewis.betechtest.products.response.Display;
import com.johnlewis.betechtest.products.response.Price;
import com.johnlewis.betechtest.products.response.Product;
import com.johnlewis.betechtest.products.response.ProductDto;
import com.johnlewis.betechtest.products.response.ReductionHistoryDto;
import com.johnlewis.betechtest.products.response.VariantPriceRangeDto;
import org.junit.jupiter.api.Test;

import java.util.Collections;
import java.util.List;

import static org.assertj.core.api.AssertionsForInterfaceTypes.assertThat;
import static org.junit.jupiter.api.Assertions.assertNull;

class ProductMapperTest {

    private static final String PRODUCT_ID = "productId";
    private static final String TITLE = "title";
    private static final String WAS = "£15.00";
    private static final String THEN = "£10.00";
    private static final String NOW = "£5.00";

    private final ProductMapper productMapper = ProductMapper.INSTANCE;

    @Test
    void shouldMapProductWasThenNow() {
        ProductDto productDto = new ProductDto(
                PRODUCT_ID, TITLE,
                buildVariantPriceRangeDto(WAS, THEN, NOW)
        );

        Product product = new Product(
                PRODUCT_ID, TITLE,
                new Price(WAS, THEN, NOW)
        );

        Product result = productMapper.toProduct(productDto);

        assertThat(result).isEqualTo(product);
    }

    @Test
    void shouldMapProductWasNow() {
        ProductDto productDto = new ProductDto(
                PRODUCT_ID, TITLE,
                buildVariantPriceRangeDto(WAS, null, NOW)
        );

        Product product = new Product(
                PRODUCT_ID, TITLE,
                new Price(WAS, null, NOW)
        );

        Product result = productMapper.toProduct(productDto);

        assertThat(result).isEqualTo(product);
    }

    @Test
    void shouldMapProductNow() {
        ProductDto productDto = new ProductDto(
                PRODUCT_ID, TITLE,
                buildVariantPriceRangeDto(null, null, NOW)
        );

        Product product = new Product(
                PRODUCT_ID, TITLE,
                new Price(null, null, NOW)
        );

        Product result = productMapper.toProduct(productDto);

        assertThat(result).isEqualTo(product);
    }

    @Test
    void shouldProvideNullResponseForEmptyObjectWhenMappingToPrice() {
        VariantPriceRangeDto variantPriceRangeDto = new VariantPriceRangeDto(null, null);
        assertNull(productMapper.toPrice(variantPriceRangeDto));
    }

    @Test
    void shouldProvideNullResponseForEmptyObjectWhenMappingToProduct() {
        ProductMapper productMapper = ProductMapper.INSTANCE;
        assertNull(productMapper.toProduct(null));
    }

    private VariantPriceRangeDto buildVariantPriceRangeDto(String was, String then, String now) {
        if (now != null && was != null && then != null) {
            return new VariantPriceRangeDto(
                    new Display(now), List.of(
                    new ReductionHistoryDto(0, new Display(was)),
                    new ReductionHistoryDto(1, new Display(then))
            ));
        } else if (now != null && was != null) {
            return new VariantPriceRangeDto(
                    new Display(now), List.of(
                    new ReductionHistoryDto(0, new Display(was))
            ));
        } else if (now != null) {
            return new VariantPriceRangeDto(
                    new Display(now), Collections.emptyList()
            );
        } else {
            return new VariantPriceRangeDto(null, null);
        }
    }
}
