package com.johnlewis.betechtest.products.service;

import com.johnlewis.betechtest.products.mapper.ProductMapper;
import com.johnlewis.betechtest.products.mapper.ProductMapperImpl;
import com.johnlewis.betechtest.products.response.Display;
import com.johnlewis.betechtest.products.response.Product;
import com.johnlewis.betechtest.products.response.ProductDto;
import com.johnlewis.betechtest.products.response.ProductResponse;
import com.johnlewis.betechtest.products.response.ReductionHistoryDto;
import com.johnlewis.betechtest.products.response.VariantPriceRangeDto;
import org.junit.jupiter.api.Test;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.when;

class DressesServiceTest {
    private static final String PRODUCT_ID = "productId";
    private static final String PRODUCT_ID_2 = "productId2";
    private static final String TITLE = "title";
    private static final String WAS = "£10.00";
    private static final String NOW = "£2.00";

    private final RestTemplate restTemplate = mock(RestTemplate.class);
    private final ProductMapper productMapper = spy(ProductMapperImpl.class);
    private final DressesService dressesService = new DressesService(restTemplate, productMapper, "apiUrl");

    @Test
    void shouldReturnEmptyListWhenExceptionThrown() {

        when(restTemplate.exchange(anyString(), any(HttpMethod.class), any(HttpEntity.class), any(Class.class))).thenThrow(RestClientException.class);

        List<Product> products = dressesService.getDresses();
        assertNotNull(products);
        assertEquals(0, products.size());
    }

    @Test
    void shouldReturnPartialListWhenExceptionThrownDuringProductMapping() {
        ProductDto productDto = new ProductDto(
            PRODUCT_ID_2,
            TITLE,
            buildVariantPriceRangeDto()
        );
        List<ProductDto> productDtos = getProductDtos();
        productDtos.add(productDto);
        ProductResponse productResponse = new ProductResponse(productDtos);
        ResponseEntity<ProductResponse> responseEntity = new ResponseEntity<>(productResponse, HttpStatus.OK);
        when(restTemplate.exchange(anyString(), any(HttpMethod.class), any(HttpEntity.class), any(Class.class))).thenReturn(responseEntity);
        when(productMapper.toProduct(productDto)).thenThrow(new RuntimeException());

        List<Product> products = dressesService.getDresses();
        assertNotNull(products);
        assertEquals(1, products.size());
    }

    @Test
    void shouldReturnEmptyListAsEmptyResponse() {

        ProductResponse productResponse = null;
        ResponseEntity<ProductResponse> responseEntity = new ResponseEntity<>(productResponse, HttpStatus.OK);
        when(restTemplate.exchange(anyString(), any(HttpMethod.class), any(HttpEntity.class), any(Class.class))).thenReturn(responseEntity);

        List<Product> products = dressesService.getDresses();
        assertNotNull(products);
        assertEquals(0, products.size());
    }

    @Test
    void shouldReturnEmptyListAsProductListEmpty() {
        ProductResponse productResponse = new ProductResponse(Collections.emptyList());
        ResponseEntity<ProductResponse> responseEntity = new ResponseEntity<>(productResponse, HttpStatus.OK);
        when(restTemplate.exchange(anyString(), any(HttpMethod.class), any(HttpEntity.class), any(Class.class))).thenReturn(responseEntity);

        List<Product> products = dressesService.getDresses();
        assertNotNull(products);
        assertEquals(0, products.size());
    }

    @Test
    void shouldReturnProducts() {
        ProductResponse productResponse = new ProductResponse(getProductDtos());
        ResponseEntity<ProductResponse> responseEntity = new ResponseEntity<>(productResponse, HttpStatus.OK);
        when(restTemplate.exchange(anyString(), any(HttpMethod.class), any(HttpEntity.class), any(Class.class))).thenReturn(responseEntity);

        List<Product> products = dressesService.getDresses();
        assertNotNull(products);
        assertEquals(1, products.size());
    }

    private List<ProductDto> getProductDtos() {
        final List<ProductDto> productDtos = new ArrayList<>();
        productDtos.add(new ProductDto(
            PRODUCT_ID,
            TITLE,
            buildVariantPriceRangeDto()
        ));
        return productDtos;
    }

    private VariantPriceRangeDto buildVariantPriceRangeDto() {
        return new VariantPriceRangeDto(
            new Display(NOW), List.of(
            new ReductionHistoryDto(0, new Display(WAS))));
    }
}
