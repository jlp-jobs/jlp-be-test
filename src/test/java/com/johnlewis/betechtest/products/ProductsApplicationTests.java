package com.johnlewis.betechtest.products;

import com.johnlewis.betechtest.products.response.Price;
import com.johnlewis.betechtest.products.response.Product;
import com.johnlewis.betechtest.products.service.DressesService;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.net.URL;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class ProductsApplicationTests {

    private static final String PRODUCT_ID = "productId";
    private static final String TITLE = "title";
    private static final String WAS = "15.00";
    private static final String THEN = "10.00";
    private static final String NOW = "5.00";
    private static final Price PRICE = new Price(WAS, THEN, NOW);

    private String url;

    @LocalServerPort
    private int port;

    @Autowired
    private TestRestTemplate restTemplate;

    @MockBean
    private DressesService dressesService;

    @BeforeEach
    void before() {
        url = "http://localhost:" + port;
    }

    @AfterEach
    void after() {
        verifyNoMoreInteractions(dressesService);
    }

    @Test
    public void getDresses() throws Exception {
        String path = "/products/dresses";
        given(dressesService.getDresses()).willReturn(List.of(new Product(PRODUCT_ID, TITLE, PRICE)));
        String expectedProductArray = "[{" +
                "\"productId\":\"" + PRODUCT_ID + "\"," +
                "\"title\":\"" + TITLE + "\"," +
                "\"price\":{\"was\":\"" + WAS + "\",\"then\":\"" + THEN + "\",\"now\":\"" + NOW + "\"}" +
                "}]";

        ResponseEntity<String> response = restTemplate.getForEntity(new URL(url + path).toString(), String.class);

        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertEquals(expectedProductArray, response.getBody());
        verify(dressesService).getDresses();
    }
}
