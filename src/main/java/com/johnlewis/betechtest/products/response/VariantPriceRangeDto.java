package com.johnlewis.betechtest.products.response;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

import java.util.List;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class VariantPriceRangeDto {
    private final Display display;
    private final List<ReductionHistoryDto> reductionHistory;
}
