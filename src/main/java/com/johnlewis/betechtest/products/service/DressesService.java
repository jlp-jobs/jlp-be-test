package com.johnlewis.betechtest.products.service;

import com.johnlewis.betechtest.products.mapper.ProductMapper;
import com.johnlewis.betechtest.products.response.Product;
import com.johnlewis.betechtest.products.response.ProductDto;
import com.johnlewis.betechtest.products.response.ProductResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Service
public class DressesService {
    private final String johnLewisDressApi;
    private final RestTemplate restTemplate;
    private final ProductMapper productMapper;

    @Autowired
    DressesService(RestTemplate restTemplate, ProductMapper productMapper, @Value("${jl.api.url}") String apiUrl) {
        this.restTemplate = restTemplate;
        this.productMapper = productMapper;
        this.johnLewisDressApi = apiUrl;
    }

    public List<Product> getDresses() {
        ProductResponse productResponse = null;
        try {
            HttpHeaders headers = new HttpHeaders();
            headers.set("Accept", MediaType.APPLICATION_JSON_VALUE);
            headers.set("User-Agent", "Back-end tech test");
            HttpEntity<String> entity = new HttpEntity<>(headers);
            productResponse = restTemplate.exchange(
                    johnLewisDressApi,
                    HttpMethod.GET,
                    entity,
                    ProductResponse.class)
                .getBody();
        } catch (RestClientException e) {
            System.out.println(e);
        }
        final List<Product> products = new ArrayList<>();
        if (productResponse != null && productResponse.getProducts() != null) {
            final List<Product> mappedProducts = productResponse.getProducts().stream()
                .map(this::errorHandledProductMapping)
                .filter(Objects::nonNull)
                .collect(Collectors.toList());
            products.addAll(mappedProducts);
        }
        return products;
    }

    private Product errorHandledProductMapping(final ProductDto productDto) {
        try {
            return productMapper.toProduct(productDto);
        } catch (final Exception e) {
            System.out.println(e);
            return null;
        }
    }
}
